Youdao.alfredworkflow
======================

有道翻译 workflow for Alfred v2

默认快捷键 f，多个翻译结果，回车或直接点击将结果复制到剪切板。

## [Download](https://github.com/samqiu/Youdao.alfredworkflow/raw/master/Youdao.alfredworkflow)

## Screenshot

![youdao](https://f.cloud.github.com/assets/290421/301401/9402b7de-95d1-11e2-9859-f267cf16642d.gif)
